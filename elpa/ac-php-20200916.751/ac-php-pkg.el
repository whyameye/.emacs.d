;;; -*- no-byte-compile: t -*-
(define-package "ac-php" "20200916.751" "Auto Completion source for PHP." '((ac-php-core "2.0") (auto-complete "1.4.0") (yasnippet "0.8.0")) :commit "933805013e026991d29a7abfb425075d104aa1cf" :authors '(("jim" . "xcwenn@qq.com")) :maintainer '("jim") :keywords '("completion" "convenience" "intellisense") :url "https://github.com/xcwen/ac-php")
